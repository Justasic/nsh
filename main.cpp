// Copyright (c) 2022 Justin Crawford <OpenSource@stacksmash.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include <cstdio>
#include <iostream>
#include <vector>
#include <string>
#include <string_view>
#include <ranges>
#include <iomanip>
#include <filesystem>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <fmt/format.h>
#include <fmt/ranges.h>

// This function will parse the string given from user's input
// and split it into an array. This array is later passed to the
// command function. 
// 
// All this does is take a string (say: "Hello my name is john")
// and splits it into an array that looks like:
// ["Hello", "my", "name", "is", "john"]
// But because some commands expect an individual token to be a
// space-containing string, there's a special case to allow quoted
// strings. An example: "Hello, 'this is a quoted string'" should
// render the following array:
// ["Hello,", "this is a quoted string"]
// 
// The [[nodiscard]] is a C++ attribute used to tell the compiler that
// if the returned value is not used (aka, we don't assign it to a variable
// in the calling function) that the compiler should emit a warning.
[[nodiscard]] std::vector<std::string> tokenize(std::string_view input)
{
	// Some variables we use to check if
	// we're inside a quoted text block or not
	bool in_double_quote = false;
	bool in_single_quote = false;
	std::vector<std::string> args;

	// A buffer to hold our token while we iterate the string.
	std::string buffer;
	// Iterate the character string, character by character
	for (auto ch : input)
	{
		if (ch == '"') // Is this char a double quote? (aka: ")
			in_double_quote = !in_double_quote;
		else if (ch == '\'') // Is this char a single quote? (aka: ')
			in_single_quote = !in_single_quote;
		// If we're not in any kinds of quotes and we've reached a space character
		// then insert our copied text into the vector. 
		// I will explain the static_cast below.
		else if (ch == ' ' && !in_double_quote && !in_single_quote)
			args.push_back(static_cast<decltype(buffer)&&>(buffer));
		else // Otherwise, copy the character into the buffer for this token
			buffer += ch;
	}
	
	// Make sure any remnants in the buffer are appended as the
	// last token, otherwise we get something like:
	// "hello, good day sir"
	// becomes
	// ["hello,", "good", "day"]
	// which is obviously not what we want.
	//
	// The static_cast<decltype(buffer)&&> is a unique way to move
	// objects. This cast can be simplified to: static_cast<std::string&&>
	// since std::string is the type of the `buffer` variable.
	// In C++, the && operator means "rvalue reference", while that's a confusing
	// name, what it really means is that this object can be "moved" to another
	// location. "Moved" is quite literal here, by default C++ will make copies of
	// variables, which can be slow and unwanted. When you put a string inside
	// std::string, it will call malloc() internally which is very slow and uses
	// memory. If we did `args.push_back(buffer)` (which is valid) then C++ will
	// make a copy of `buffer` before inserting into `args`. When you copy an
	// std::string, it will call malloc() because you are inserting a string into
	// a new std::string object to insert it into `args`. Obviously we don't want
	// to make a copy of `buffer` since we're never using it after this point and
	// the old copy will be destroyed when this function returns.
	// By making this static cast, you force C++ to call the "move constructor" of
	// std::string, which will move the contents of `buffer` into `args` and thus
	// we avoid copying. This static_cast can be directly replaced with std::move:
	// `args.push_back(std::move(buffer))` which does exactly the same thing. To
	// understand why I prefer the cast over std::move, please read this article:
	// https://www.foonathan.net/2020/09/move-forward/
	if (!buffer.empty())
		args.push_back(static_cast<decltype(buffer)&&>(buffer));

	// Uh oh, looks like the user never terminated the string!
	// Best to not process the command since it's likely an error.
	// More advanced shells might allow multi-line input but we're
	// too dumb and simple for that.
	if (in_double_quote || in_double_quote)
	{
		std::cout << "Incomplete quoted string" << std::endl;
		return {};
	}

	return args;
}

/*
 * Run a separate process and return it's exit code.
 * The process that is started is specified as the
 * 0th item in `args` provided to the function.
 */
[[nodiscard]] int cmd(std::vector<std::string> &&args)
{
	// it is invalid to execute nothing.
	if (args.empty())
		return -1;

	// On POSIX systems (Linux, Android, FreeBSD, MacOS, etc.)
	// a process is spawned by forking (cloning) itself, then
	// calling exec() (or similar function) to actually run
	// the program. Fork works by copying the entire process's 
	// memory space, open file handles, everything and making a
	// near identical clone but the kernel gives this copy a
	// new pid. If that new pid is returned by fork() then we
	// know that we're the parent process (the one executing the
	// command), if fork return 0, then we know that we're the
	// child process (we're the command being executed). If fork
	// returns a negative number, then something went wrong and
	// we should check `errno` for an error code.
	pid_t pid = fork();

	if (pid > 0)
	{
		// we're the parent, wait for the child to finish
		// execution, crash, or otherwise go away.
		int childcode = 0;
		wait(&childcode);

		// if the child exited normally, just return it's
		// exit code. In the child, this is that number that
		// you return from `int main()` and can be used for
		// error conditions if the command didn't execute
		// properly.
		if (WIFEXITED(childcode))
			return WEXITSTATUS(childcode);
		else if (WIFSIGNALED(childcode))
		{
			// If the child exited because it got a signal
			// then usually that means something bad happened.
			// The child crashed, was killed, or otherwise didn't
			// handle the error condition on it's own so it was
			// terminated. 
			int sigid = WTERMSIG(childcode);

			// Here we print some "useful" messages on what happened to
			// the child process instead of just returning a blank response
			// and not knowing if the command was successful or not.
			switch(sigid)
			{
				case SIGABRT:
					std::cout << "Bring out the coat hanger because child was aborted!" << std::endl;
					break;
				case SIGSEGV:
					std::cout << "The child had a f̵̹̪̙̖᷊̄͠u̶̧̧̢͉̘͈̙᷁̐ͩ̐͒̀͑̈́ͭ̂c̵̮̙̻̋k̵̖̱̲̲̹̮̤̳ͤ᷆͑ͯ͛͜͟͝y̴̘̟᷊̭̝̖̹̋̍͛̐ͧ︠᷾ w̵̞̼̘̣᷿͋͊u̷̧͓̞̖͓᷿̯̯͉᷈͌̀͟c̷̲̀̀̉᷾︡k̸̤͕᷅ͬ̽̃̈́︣ͨ︣̃ͥẏ̸̞̰̰͉̰̈̉︡͗ͪ᷄ͨ̚̚" << std::endl;
					break;
				case SIGKILL:
					std::cout << "The child was murdered." << std::endl;
					break;
				case SIGILL:
					std::cout << "The child committed a felony." << std::endl;
					break;
				case SIGPIPE:
					std::cout << "The child walked into a bar.\n\nThat's it.\n\nThey walked into a bar and died." << std::endl;
					break;
				case SIGQUIT:
					std::cout << "Damn millenial can't keep a job..." << std::endl;
					break;
				case SIGBUS:
					std::cout << "Child is no longer bussin' cuh" << std::endl;
					break;
				case SIGCHLD:
					std::cout << "Grandchildren were murdered." << std::endl;
					break;
				case SIGTERM:
					std::cout << "The child's life was terminated" << std::endl;
					break;
				default:
					std::cout << "The child got a signal from kernel daddy: " << sigid << std::endl;
					break;
			};
		}
		else // If something else happened (eg, child was suspended or something) then print a message as well.
			std::cout << "Something happened to the child but I can't care enough to figure out what." << std::endl;
		return -1;
	}
	else if (pid == 0)
	{
		// we're the child, call execvp to execute the process.
		
		// Because execvp() takes an array of character pointers (aka: char**, remember
		// how `int main()` has `argv` which is a char**? that's where this comes from)
		// and not a std::vector<std::string>, we must convert all the std::string's from
		// the vector and put them into a char**, but first we need to allocate space
		// for the array of pointers, so we take how many std::string's there are in
		// the `args` vector, add one more because the last item in the list has to be
		// a null pointer which is required by the `execvp()` function to know where the
		// end of the array is. We take this number (it could be 2 or 3 items total) and
		// multiply that by the size of a character pointer type. This size is different
		// from machine to machine but generally 64 bit machines have a pointer size of
		// 8 bytes, while 32 bit machines have a size of 4 bytes. We now have our array
		// of pointers in C code converted from C++ containers.
		char **argv = static_cast<char**>(malloc((args.size() + 1) * sizeof(char*)));
		// Now we copy the pointers from the std::string inside the std::vector array
		// to the C array. Do note that if std::vector goes out of scope of this if-statement
		// that it would deallocate and make everything inside our C array invalid. You
		// should never store pointers in a separate array like this because std::string
		// will free() (aka, deallocate) the internal pointer.
		// I will explain why I am doing this (when you should not do it) below.
		for (size_t i = 0; i < args.size(); ++i)
			argv[i] = args[i].data();
		// execvp() requires that the last item in the array is a null pointer
		// so set that here.
		argv[args.size()+1] = nullptr;

		// In POSIX systems, execvp() will load the executable file from the filesystem
		// and setup a new runtime space, reallocating *all* process memory space to be
		// what you would expect from a brand new `int main()` elsewhere. Because the
		// entire process's memory space is reallocated by the kernel, we don't need to
		// worry about any lost or unfreed memory, it will be overwritten anyway as the
		// child process continues to run.
		// execvp() will never return as well, because remember we're a separate process
		// since calling fork(), we can just call exit(0) if we wanted to and the parent
		// process would receive a status from calling wait() in the above if-condition.
		// The child executable will call exit() directly themselves so the only time
		// we have execvp() return is if it failed to execute the child process for some
		// reason, and in that case we can print an error for the user.
		execvp(argv[0], argv);
		// this free is unnecessary because we're a separate process and we're
		// about to exit (so the kernel will reclaim our memory anyway) but it
		// is good practice to clean up your memory as you use it.
		free(argv);
		std::cout << "Uh oh daddy, I can't do that! Kernel daddy says " << std::quoted(strerror(errno)) << std::endl;
		// Again, because we're a child process from fork(), we actually exit our process
		// instead of returning from this function. We only want 1 shell, not 2.
		exit(1);
	}
	else
	{
		// fork() failed, so we should explain that to the user
		std::cout << "A fucky wucky happened: " << strerror(errno) << std::endl;
		return -1;
	}

	// This should be unreachable but it's here to make sure the compiler
	// is happy and that if we mess up anywhere else, we at least return
	// a sane value instead of corrupting the stack.
	return 0;
}

// This function will render a shell prompt for the user
// based on where the current directory and last run command's
// exit code was.
void RenderPrompt(int retcode = 0)
{
	// Get our current path
	std::filesystem::path cwd = std::filesystem::current_path();

	// Get the home dir
	char *h = getenv("HOME");
	std::filesystem::path home;
	if (h)
		home = h;

	std::string path = cwd.native();
	// Check if it's a subdir of home.
	if (int cmp = cwd.compare(home); cmp >= 0)
	{
		path = "~";
		if (cwd.compare(home) >= 0)
			path += "/" + cwd.lexically_relative(home).native();
	}

	// This is a unique prompt, if a previous command had a non-zero exit code
	// then render it as apart of the prompt, else omit the exit code entirely.
	// The below lines use libfmt (see: https://fmt.dev/ for it's usage)
	if (retcode)
		fmt::print("[nsh|{}] {}> ", retcode, path);
	else
		fmt::print("[nsh] {}> ", path);
}

// The program's main entry point!
int main([[maybe_unused]] int argc, [[maybe_unused]] char **argv)
{
	// Disable buffering
	std::cout.rdbuf()->pubsetbuf(0, 0);
	std::setbuf(stdout, nullptr);

	// Render the prompt
	RenderPrompt();
	// Wait for input, this also removes the newline from the user hitting enter to run the command
	for (std::string input; std::getline(std::cin, input);)
	{
		// user wants to exit this shell.
		if (input == "exit" || input == "quit")
			return 0;

		// return value of previous process.
		int ret{0};

		// Split the string into a vector
		std::vector args = tokenize(input);

		// Process changing directory as a built-in command
		// TODO: this should be a separate function
		if (!args.empty() && args[0] == "cd")
		{
			if (chdir(args[1].c_str()) == -1)
			{
				fmt::print("Sorry daddy, I can't take you there :( Daddy kernel said \"{}\"!\n", strerror(errno));
				ret = 1;
			}
		}
		else if (!args.empty() && args[0] == "exec")
		{
			// Erase the word "exec" from the args.
			args.erase(args.begin());
			// Replace our shell with the command.
			char **argv = static_cast<char**>(malloc((args.size()+1) * sizeof(char*)));
			for (size_t i = 0; i < args.size(); ++i)
				argv[i] = args[i].data();

			argv[args.size()+1] = nullptr;
			// Bye bye
			execvp(argv[0], argv);
			// this shouldn't happen, free the memory and return to
			// a prompt. Note that we're not fork()ed so we don't
			// call exit() because we're the same process.
			free(argv);
			fmt::print("Uh oh! A fucky wucky happened daddy! We couldn't exec that because \"{}\"\n", strerror(errno));
		}
		else
			// Execute a subprocess
			ret = cmd(static_cast<decltype(args)&&>(args));

		// Subprocess exited, re-render our prompt
		RenderPrompt(ret);
	}

	// Exit cleanly if we 
	return 0;
}
