CXX = clang++
CFLAGS = -std=c++20 -Wall -Wextra -Wpedantic -Wconversion -g
LDFLAGS = -lfmt

SRCS := $(shell find . -name '*.cpp')
OBJS := $(SRCS:.cpp=.o)

%.o: %.cpp
	$(CXX) $(CFLAGS) -c $< -o $@

nsh: $(OBJS)
	$(CXX) $(CFLAGS) -o $@ $(OBJS) $(LDFLAGS)

#clang++ -std=c++20 -Wall -Wextra -Wpedantic -Wconversion -g -o main main.cpp -lfmt
