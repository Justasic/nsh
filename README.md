NightShadow's Shell
===

This is a very, very basic command line shell used for executing commands (like bash, zsh, sh, csh, etc.) in a POSIX system.
The shell doesn't have any special support outside of launching some basic programs, there's no scripts, no piping, othing
special. It's primarily used to demonstrate how to write a shell and how to write C++.

Compiling
===

Compiling is easy on a non-windows system:

```
$ make
```

An executable named `nsh` will be available, run it and it will launch the shell.

On windows, you must either use mingw or WSL to compile this shell. There is no windows support directly.
